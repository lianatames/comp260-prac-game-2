﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class MovePaddle : MonoBehaviour {

	private Rigidbody rigidbody;
	public float speed = 20f;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void FixedUpdate () {
//		Vector2 direction;
//		direction.x = Input.GetAxis ("Horizontal");
//		direction.y = Input.GetAxis ("Vertical");
//
//		Vector2 velocity = direction * speed;
//
//		rigidbody.AddForce(velocity * Time.fixedDeltaTime);

		Vector3 pos = GetMousePosition();
		Vector3 dir = pos - rigidbody.position;
		Vector3 vel = dir.normalized * speed;

		// check is this speed is going to overshoot the target
		float move = speed * Time.fixedDeltaTime;
		float distToTarget = dir.magnitude;

		if (move > distToTarget) {
			// scale the velocity down appropriately
			vel = vel * distToTarget / move;
		}

		rigidbody.velocity = vel;
	}


	private Vector3 GetMousePosition() {
		// create a ray from the camera 
		// passing through the mouse position
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		// find out where the ray intersects the XY plane
		Plane plane = new Plane(Vector3.forward, Vector3.zero);
		float distance = 0;
		plane.Raycast(ray, out distance);
		return ray.GetPoint(distance);
	}

	void OnDrawGizmos() {
		// draw the mouse ray
		Gizmos.color = Color.yellow;
		Vector3 pos = GetMousePosition();
		Gizmos.DrawLine(Camera.main.transform.position, pos);
	}

}

